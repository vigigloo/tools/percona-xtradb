variable "dbuser" {
  type    = string
  default = null
}

variable "dbname" {
  type    = string
  default = null
}

variable "dbpassword" {
  type    = string
  default = null
}

resource "random_string" "user" {
  count   = var.dbuser == null ? 1 : 0
  length  = 32
  special = false
}

resource "random_string" "name" {
  count   = var.dbname == null ? 1 : 0
  length  = 32
  special = false
}

resource "random_password" "password" {
  count  = var.dbpassword == null ? 1 : 0
  length = 128
}

locals {
  dbpassword = var.dbpassword == null ? random_password.password[0].result : var.dbpassword
  dbuser     = var.dbuser == null ? random_string.user[0].result : var.dbuser
  dbname     = var.dbname == null ? random_string.name[0].result : var.dbname
  dbhost     = "${var.chart_name}-pxc-db-haproxy.${var.namespace}.svc.cluster.local"
}

resource "kubernetes_job" "user_db_creation" {
  depends_on = [helm_release.percona-xtradb]
  metadata {
    name      = "${var.chart_name}-user-db-creation"
    namespace = var.namespace
  }

  spec {
    template {
      metadata {
      }

      spec {
        container {
          name  = "mysql"
          image = "mysql:5.7"

          env {
            name = "MYSQL_ROOT_PASSWORD"
            value_from {
              secret_key_ref {
                name = "${var.chart_name}-pxc-db"
                key  = "root"
              }
            }
          }

          env {
            name  = "MYSQL_DATABASE"
            value = local.dbname
          }

          env {
            name  = "MYSQL_USER"
            value = local.dbuser
          }

          env {
            name  = "MYSQL_PASSWORD"
            value = local.dbpassword
          }

          env {
            name  = "MYSQL_HOST"
            value = local.dbhost
          }

          command = ["/bin/bash", "-c"]
          args = [
            <<-EOT
            mysql -h $MYSQL_HOST \
            -u root -p"$MYSQL_ROOT_PASSWORD" \
            -e "CREATE DATABASE $MYSQL_DATABASE; \
                CREATE USER $MYSQL_USER@\"%\" IDENTIFIED BY \"$MYSQL_PASSWORD\"; \
                GRANT ALL PRIVILEGES ON $MYSQL_DATABASE.* TO $MYSQL_USER@\"%\" \
               "
            EOT
          ]
        }

        restart_policy = "Never"
      }
    }

    ttl_seconds_after_finished = "10800"
    backoff_limit              = 3
  }
}

output "database" {
  value = local.dbname
}

output "username" {
  value = local.dbuser
}

output "password" {
  value = local.dbpassword
}

output "host" {
  value = local.dbhost
}
